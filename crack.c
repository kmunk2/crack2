#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *dict = fopen(dictionary, "r");
    
    char sample[PASS_LEN];
    char hash[HASH_LEN];
    char output[PASS_LEN]  = "\n[NOT FOUND]";
    // Loop through the dictionary file, one line
    // at a time.
    
    //printf("%s\n", target);
    
    while(fgets(sample,PASS_LEN,dict) != NULL){
        
        for(int i = 0; i<strlen(sample); i++){
            if(sample[i] == '\n'){
                sample[i] = 0;
            }
        }
        
        char *test = md5(sample, strlen(sample));
        //printf("%s\n", test);
        
        if(strcmp(test,target) == 0){
            //sprintf("Found\n");
            for(int i = 0; i<strlen(test); i++){
                output[i] = sample[i];
            }
        }
        
    }
    // Hash each password. Compare to the target hash.
    // If they match, return the corresponding password.
    
    // Free up memory?

    return(output);
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hashes = fopen(argv[1], "r");
    char hash[HASH_LEN];
    
    // For each hash, crack it by passing it to crackHash
    while(fgets(hash, HASH_LEN, hashes) != NULL){
        
        for(int i = 0; i<strlen(hash); i++){
            if(hash[i] == '\n'){
                hash[i] = 0;
            }
        }
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        
        if(strlen(hash) != 0){
                
            char *result = crackHash(hash, "rockyou1m.txt");
            
            printf("%s = %s\n",hash , result);
        }
        
    }
    
    fclose(hashes);
    // Close the hash file
    
    // Free up any malloc'd memory?
}
